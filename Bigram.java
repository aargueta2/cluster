import java.util.HashMap;
import java.util.Set;

public class Bigram {
	private HashMap<String, Counter<String>> counts;
	
	public Bigram(){
		this.counts = new HashMap<>();
	}
	
	/**
	 * Add a bigram (current, next).
	 * @param current  first word
	 * @param next     second word
	 */
	public void addBigram(String current, String next){
		if (counts.containsKey(current)) {
		    counts.get(current).incrementCount(next);
		}
		else {
			Counter<String> innerCounts = new Counter<String>();
		    innerCounts.addCount(next, 1);
		    counts.put(current, innerCounts);
		}
	}

	/**
	 * Return a collection of the word types that are the first word of a bigram.
	 */
	public Set<String> keySet(){
		return this.counts.keySet();
	}

	/**
	 * Return a collection of the word types that follow "current".
	 * @param current  first word
	 */
	public Set<String> followingWords(String current){
		if (!counts.containsKey(current))
			return null;
		Counter<String> words_return = counts.get(current);
		return words_return.keySet();
	}
	
	/**
	 * Return the count of a certain bigram (current, next).
	 * @param current  first word
	 * @param next     second word
	 * @return the count of the bigram, or 0 if bigram does not exist.
	 */
	public int count(String current, String next){
		if (counts.containsKey(current)) 
			if (counts.get(current).containsKey(next))
				return counts.get(current).getCount(next);
		return 0;
	}

	/**
	 * Return the unigram count of "current", or, more precisely, the number of times that "current" appears as the first word of a bigram.
	 */
	public int unigramCount(String current) {
		return counts.get(current).getTotalCount();
	}
	
	/**
	 * Print all the bigrams and their counts.
	 */
	public void printCounts(){
		for(String str: counts.keySet()){
			System.out.println("Current String is: "+str);
			for(String str2:counts.get(str).keySet()){
				System.out.println(""+str2+" With count: "+counts.get(str).getCount(str2));
			}
			System.out.println("-------------------------");
		}
	}
}
