import java.util.*;

public class Clustering_Algo_Fast {
    int k;
    int total_num_words; // TODO: Move into Bigrams?
    ArrayList<Cluster> all_clusters;
    Bigram info_all_words;

    /*
     * Fast version of clustering algorithm.
     * @param k		The desired number of clusters.
     * @param bigrams	Bigram statistics.
     */
    public Clustering_Algo_Fast(int k, Bigram bigrams) {
        this.k = k;
        this.info_all_words = bigrams;

	// Build the initial cluster.
	Cluster cluster = new Cluster(bigrams);
        total_num_words = 0;
	for (String a : bigrams.keySet()) {
	    total_num_words += bigrams.unigramCount(a);
	    cluster.addWord(a, bigrams.unigramCount(a));
	}

	System.out.println("Size of initial cluster: " + cluster.words.size());

        this.all_clusters = new ArrayList<Cluster>();
        this.all_clusters.add(cluster);

	System.out.println("initial H (slow) = " + calculate_h_forward());
    }

    public void start(){
        //\For{$i \leftarrow 1, \ldots, \log_2{k}$}
        for(int i=1;i<=((float)Math.log(k)/(float)Math.log(2));i++){
            //\For{each cluster $\bar{x}$}
            while(this.all_clusters.size() < k){
                int n = all_clusters.size();
                for(int j = 0 ; j < n ; j ++){
                    Cluster first = all_clusters.get(0);
                    split(first);
                    for(Cluster a: this.all_clusters){
                        System.out.println("Size of cluster: "+a.words.size());
                    }
                }
            }
        }

        //Optimize section/method
        //FINE TUNE PART OF THE ALGORITHM
        for(int j=0;j<4;j++){
            int theCluster_size = all_clusters.size();
            for(int first_cluster = 0 ;first_cluster < theCluster_size;first_cluster++){
                for(int second_cluster = 0;second_cluster<theCluster_size;second_cluster++){
                    if(first_cluster != second_cluster){
                        this.optimize(all_clusters.get(first_cluster), all_clusters.get(second_cluster));

                    }
                }
            }
        }
        printCluster(all_clusters);
    }

    /**
     * Split all the elements and create two clusters
     * return an array of 2 clusters
     * TODO: "Buckshot" splitting
     */
    public void split(Cluster x_bar){
	System.out.println("split");

        Cluster [] splitCluster = new Cluster[2];
        splitCluster[0] = new Cluster(info_all_words);
        splitCluster[1] = new Cluster(info_all_words);
        int clusterSize = x_bar.word_count.size() / 2;
        int iterator_cluster_one =  0;
        int iterator_cluster_two = 0;

        Random randomCluster = new Random();
        //TODO: Remove the random seed
        randomCluster.setSeed(20);
        for (String key : x_bar.word_count.keySet()) {
            int clusterSelected = randomCluster.nextInt();

            //distribute words randomly between the clusters
            if((clusterSelected%2 == 0 && iterator_cluster_one < clusterSize) || iterator_cluster_two > clusterSize){
                splitCluster[0].addWord(key,x_bar.word_count.get(key));
                iterator_cluster_one++;
            }
            else{
                splitCluster[1].addWord(key,x_bar.word_count.get(key));
                iterator_cluster_two++;
            }
        }

        this.all_clusters.remove(x_bar);
        this.all_clusters.add(splitCluster[0]);
        this.all_clusters.add(splitCluster[1]);
        optimize(splitCluster[0], splitCluster[1]);
        System.out.println("-----------------------------");
    }


    /**
     * method to optimize two clusters of words
     * @param x first cluster
     * @param y second cluster
     * @return two optimized clusters (if any optimization is possible)
     */
    public void optimize(Cluster cluster1, Cluster cluster2){
        float h_max = calculate_h_forward();
	//System.out.println("starting H = " + h_max);

        int iter_x = cluster1.words.size();
        int iter_y = cluster2.words.size();

        for(int cluster_1 = 0 ;cluster_1<iter_x;cluster_1++){
            for(int cluster_2=0;cluster_2<iter_y;cluster_2++){
                //switch words in cluster x and y and then optimize
                String word_x = cluster1.words.get(cluster_1);
                String word_y = cluster2.words.get(cluster_2);

		//System.out.println("considering swap " + word_x + " and " + word_y);

		// TODO: Is this exact or are there any off-by-one errors?
		float dh1 = 0, dh2 = 0;
                dh1 += change_cluster_delete_word(cluster1,word_x);
                dh2 += change_cluster_add_word(cluster2, word_x);
                dh2 += change_cluster_delete_word(cluster2, word_y);
                dh1 += change_cluster_add_word(cluster1, word_y);

		//System.out.println("  dh = " + (dh1+dh2));

		if(dh1+dh2 < 0){
		    System.out.println("swap " + word_x + " and " + word_y + " => H = " + (h_max+(dh1+dh2)/total_num_words));
		    
		    // actually perform the swap
		    int probability_x = cluster1.removeWord(word_x);
		    cluster2.addWord(word_x, probability_x);
		    int probability_y = cluster2.removeWord(word_y);
		    cluster1.addWord(word_y, probability_y);

		    // This way is faster but could be prone to accumulated rounding errors
		    //h_max += (dh1+dh2)/total_num_words;

		    h_max = calculate_h_forward(); // TODO: Not necessary to recalculate all clusters
		    System.out.println("  check H: " + h_max);
		}
            }
        }
    }

    public float calculate_h_forward(){
        float total_prob = 0;

        for(Cluster a_bar: all_clusters){
            float words_in_a_bar = a_bar.word_count.getTotalCount();
            float prob_a_bar = prob_a_bar(a_bar, info_all_words, total_num_words);
            float sum_over_b = 0;
            for(String word: a_bar.count_a_bar_b.keySet()){
                int count_ab = a_bar.count_a_bar_b.getCount(word);
                float p_b_given_a = (float)count_ab / (float)words_in_a_bar;
                float calculation = p_b_given_a * ((float)Math.log(p_b_given_a)/(float)Math.log(2.0));
                if(count_ab != 0 && words_in_a_bar !=0)	{
                    sum_over_b += calculation;
                }
            }
            total_prob += -prob_a_bar*sum_over_b;
        }
        return total_prob;
    }

    /**
     * Method used to calculate the probability of a_bar
     * @param a_bar is a word cluster
     * @param previous_words object used to store the previous word of all words in the corpus
     * @return
     */
    public float prob_a_bar(Cluster a_bar,Bigram previous_words, int total_words){
	int word_count = a_bar.word_count.getTotalCount();
        return ((float)(word_count)/(float)(total_words));
    }

    /**
     * Compute the change in entropy resulting from removing word x from cluster a.
     * H'a- Ha = - ( \sum{b|c(xb)>0} (H'ab - Hab)+(c(a) - \sum{b|c(xb)>0} c(ab))*log (c(a/)/c'(a)) )
     * @param a is the cluster we modify
     * @param x is the word we will remove from the cluster
     * @return the change in H
     */
    public float change_cluster_delete_word(Cluster a, String x){
	assert a.words.contains(x);
        return change_cluster_word(a, x, -1);
    }

    /**
     * Compute the change in entropy resulting from adding word x to cluster a.
     * H'a- Ha = - ( \sum{b|c(xb)>0} (H'ab - Hab)+(c(a) - \sum{b|c(xb)>0} c(ab))*log (c(a/)/c'(a)) )
     * @param a is the cluster we modify
     * @param x is the word we will add to the cluster
     * @return the change in H
     */
    public float change_cluster_add_word(Cluster a, String x){
        return change_cluster_word(a, x, 1);
    }

    /**
     * Method to compute: H'a- Ha = \sum{b|c(xb)>0} (H'ab - Hab)+(c(a) - \sum{b|c(xb)>0} c(ab))*log (c(a)/c'(a))
     * @param a is the cluster we modify
     * @param x is the word we will add to the cluster
     * @param sign is plus or minus 1 for adding or removing
     * @return the change in H
     */
    public float change_cluster_word(Cluster a, String x, int sign){
        // Calculate the change in entropy contributed by words b that follow x:
	//   dh1 = \sum_{b|c(xb)>0} (H'_{\bar{a}b} - H_{\bar{a}b})
	// Also, while we're at it, compute:
	//   count_a_bar_b = \sum_{b|c(xb)>0} c(\bar{a}b)
	// where x = x.

        float h_prime_a_bar = 0;
        float h_a_bar = 0;
        int count_a_bar_b = 0;
	int count_x = info_all_words.unigramCount(x);
        Set<String> words_after_add = info_all_words.followingWords(x);

        if (words_after_add != null) {
            for (String b: words_after_add) {
                h_a_bar += a.calculate_h_a_bar_b(info_all_words, b);
		int count_xb = info_all_words.count(x, b);
                h_prime_a_bar += a.calculate_h_a_bar_b(info_all_words, b, sign*count_x, sign*count_xb);
                count_a_bar_b += a.count_a_bar_b.getCount(b);
            }
        }
	float dh1 = h_prime_a_bar - h_a_bar;

	// Calculate the change in entropy contributed by words b that don't follow x.

        int c_a_bar = a.word_count.getTotalCount();
        int c_prime_a_bar = c_a_bar + sign*count_x;
        float dh2 = - (c_a_bar - count_a_bar_b) * (float)(Math.log((float)c_a_bar/c_prime_a_bar)/Math.log(2));

	//System.out.println("dh = " + dh1 + " + " + dh2 + " = " + (dh1+dh2));
        return dh1 + dh2;
    }

    /**
     * Method to print cluster. This method will sort the items depending on the times they appear on the text
     * @param print
     */
    public static void printCluster(ArrayList<Cluster> print){
        for(Cluster par: print){
            par.word_count.print();
            System.out.println("   ");
        }
    }

    /**
     * Method to print a limited amount o items in the cluster
     * param list with all the clusters that will be printed
     * param limit the amount of items to print in the cluster
     */
    public static void printClusterLimited(ArrayList<Cluster> print,int limit){
        int count = 0;
        for(Cluster par: print){
            for(String ca :par.words){
                System.out.println(ca);
                if(count >= limit){
                    break;
                }
                count++;
            }
            count =0;
            System.out.println("   ");
        }
    }

    public static void printCluster_single(Cluster print){
        for(String ca :print.words){
            System.out.println(ca);

        }
        System.out.println("   ");
    }
}
