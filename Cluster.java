import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Cluster {
    	Bigram bigrams;

	// The words in this cluster. TODO: This could be eliminated, since it is redundant with count_a_bar_b, but not necessary.
	ArrayList<String> words;

	// The count of each word. TODO: Eliminate this, since it can be accessed through bigrams.unigramCount().
	Counter<String> word_count;

	// Cached counts of words following words in this cluster.
	Counter<String> count_a_bar_b;

	public Cluster(Bigram bigrams){
	    	this.bigrams = bigrams;
		this.word_count = new Counter<>();
		this.count_a_bar_b = new Counter<>();
		this.words = new ArrayList<String>();
	}

	/**
	 * Add a word to the cluster and map it
	 * @param word string to add
	 */
	public void addWord(String word,int count){
		words.add(word);
		word_count.addCount(word, count);
		for (String next: bigrams.followingWords(word))
		    count_a_bar_b.addCount(next, bigrams.count(word, next));
	}


	/**
	 * Remove the word from the cluster and remove the mapping
	 * @param word word to remove from the cluster
	 */
	public int removeWord(String word){
		if(words.contains(word)){
			int count = word_count.getCount(word);
			word_count.remove(word);
			words.remove(words.indexOf(word));
			for (String next: bigrams.followingWords(word))
			    count_a_bar_b.addCount(next, -bigrams.count(word, next));
			return count; // TODO: brittle?
		}
		//word is not in the cluster
		return 0;
	}

	/*
	 * Calculate contribution to entropy from a cluster and a following word
	 * H_{\bar{a}b}= c(\bar{a}b) * log \frac{c(\bar{a}b)}{c(\bar{a})}
	 * @param a_bar        cluster we want to analyze
	 * @param word_b       word we want to check in the cluster
	 * @return H_{\bar{a}b}
	 */
    	public float calculate_h_a_bar_b(Bigram info_all_words, String word_b) {
	    return calculate_h_a_bar_b(info_all_words, word_b, 0, 0);
	}

	/*
	 * Calculate contribution to entropy from a cluster and a following word
	 * @param a_bar        cluster we want to analyze
	 * @param word_b       word we want to check in the cluster
	 * @param add_a_bar    extra count to add to c(\bar{a})
	 * @param add_a_bar_b  extra count to add to c(\bar{a}b)
	 * @return H_{\bar{a}b}
	 */
    	public float calculate_h_a_bar_b(Bigram info_all_words, String word_b, int add_a_bar, int add_a_bar_b) {
	    int cab = count_a_bar_b.getCount(word_b)+add_a_bar_b;
	    if (cab == 0)
		return 0;
	    else {
		float result = -cab * (float)(Math.log((float)cab/(word_count.getTotalCount()+add_a_bar))/Math.log(2));
		return result;
	    }
	}
	
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
