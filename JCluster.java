import java.io.*;  
import java.util.*;

public class JCluster {
	private static void usage() {
	    System.err.println("Usage: JCluster [-slow] training-file");
	    System.exit(1);
	}

	public static void main(String args[]) throws IOException {
		// Parse command-line arguments

		if (args.length < 1) usage();

		String filename = null;
		int _fast_choice = 1; // if _fast_choice == 1 then we execute faster algo. Else slow version

		for (int i=0; i<args.length; i++) {
		    if (args[i].equals("-slow"))
			_fast_choice = 0;
		    else if (filename != null)
			usage();
		    else
			filename = args[i];
		}
		if (filename == null) usage();

		// Read input file and collect bigram statistics

		BufferedReader scan_lines = null;
		try {
		    scan_lines = new BufferedReader(new FileReader(new File(filename)));
		}
		catch (Exception e) {
		    System.err.println("Couldn't read file " + filename);
		    System.exit(1);
		}

		Bigram bigrams = new Bigram();
		String theline;
		while ((theline = scan_lines.readLine()) != null) {
			@SuppressWarnings("resource")
			Scanner scan_words = new Scanner(theline);
			String previous = "<s>";
			while (scan_words.hasNext()) {
				String s = scan_words.next();
				bigrams.addBigram(previous, s);
				previous = s;
			}
			bigrams.addBigram(previous, "</s>");
		}

		// Start clustering
		
		if (_fast_choice == 1) {
			Clustering_Algo_Fast JCluster = new Clustering_Algo_Fast(16, bigrams);
			JCluster.start();
		}
		else {
			Clustering_Algo JCluster = new Clustering_Algo(16, bigrams);
			JCluster.start();
		}
	}
}
