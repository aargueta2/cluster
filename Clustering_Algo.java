import java.util.*; 
import java.util.Map.Entry;

public class Clustering_Algo {
	float k;
	Bigram info_all_words;
	int total_num_words;
	ArrayList<Cluster> all_clusters;

	/*
	 * Slow version of clustering algorithm.
	 * @param k		The desired number of clusters.
	 * @param bigrams	Bigram statistics.
	 */
	public Clustering_Algo(float k, Bigram bigrams) {
		this.k = k;
		this.info_all_words = bigrams;

		// Build the initial cluster.
		Cluster cluster = new Cluster(bigrams);
		total_num_words = 0;
		for (String a : bigrams.keySet()) {
		    total_num_words += bigrams.unigramCount(a);
		    cluster.addWord(a, bigrams.unigramCount(a));
		}

		this.all_clusters = new ArrayList<Cluster>();
		this.all_clusters.add(cluster);
		System.out.println("Size of initial cluster: "+all_clusters.get(0).words.size());
		System.out.println("initial H = " + calculate_h_forward());
	}

	public void start(){
		for(int i=1;i<=((float)Math.log(k)/(float)Math.log(2));i++){
			//for(int j = 0 ; j < i ; j ++){
			while(this.all_clusters.size() < k){
				int n = all_clusters.size();
				for(int j = 0 ; j < n ; j ++){
					Cluster first = all_clusters.get(0); 
					split(first);
					for(Cluster a: this.all_clusters){
						System.out.println("Size of cluster: "+a.words.size());
					}
				}
			}
		}

		//Optimize section/method
		//FINE TUNE PART OF THE ALGORITHM
		for(int j=0;j<4;j++){
			System.out.println("Fine-tuning iteration " + j);
			int theCluster_size = all_clusters.size();
			for(int first_cluster = 0 ;first_cluster < theCluster_size;first_cluster++){
				for(int second_cluster = first_cluster+1;second_cluster<theCluster_size;second_cluster++){
					this.optimize(first_cluster, second_cluster);
				}
			}
		}
		printCluster(all_clusters);
	}
	
	/**
	 * Split all the elements and create two clusters
	 */
	public void split(Cluster x_bar){
		Cluster [] splitCluster = new Cluster[2];
		splitCluster[0] = new Cluster(info_all_words);
		splitCluster[1] = new Cluster(info_all_words);
		int clusterSize = x_bar.words.size() / 2;
		int iterator_cluster_one =  0; 
		int iterator_cluster_two = 0;

		System.out.println("splitting cluster");

		Random randomCluster = new Random();
        //TODO: Remove the random seed
        randomCluster.setSeed(20);
		for (String key : x_bar.words) {
			int clusterSelected = randomCluster.nextInt();

			//distribute words randomly between the clusters
			if((clusterSelected%2 == 0 && iterator_cluster_one < clusterSize) || iterator_cluster_two > clusterSize){
				splitCluster[0].addWord(key,x_bar.word_count.get(key));
				iterator_cluster_one++;
			}
			else{
				splitCluster[1].addWord(key,x_bar.word_count.get(key));
				iterator_cluster_two++;
			}
		}

		this.all_clusters.remove(x_bar);
		this.all_clusters.add(splitCluster[0]);
		this.all_clusters.add(splitCluster[1]);
		int index_cluster_1 =this.all_clusters.indexOf(splitCluster[0]);
		int index_cluster_2 =this.all_clusters.indexOf(splitCluster[1]);
		optimize(index_cluster_1, index_cluster_2);
	}


	/**
	 * Split all the elements and create two clusters
	 */
	public void split_Buckshot(Cluster x_bar){
		Cluster [] splitCluster = new Cluster[2];
		splitCluster[0] = new Cluster(info_all_words);
		splitCluster[1] = new Cluster(info_all_words);
		int clusterSize = x_bar.words.size() / 2;
		int iterator_cluster_one =  0; 
		int iterator_cluster_two = 0;
		// add the two new clusters
		this.all_clusters.add(splitCluster[0]);
		this.all_clusters.add(splitCluster[1]);
		//index of the new clusters
		int index_cluster_1 =this.all_clusters.indexOf(splitCluster[0]);

		int index_cluster_2 =this.all_clusters.indexOf(splitCluster[1]);

		Random randomCluster = new Random(); // used to remove random elements from x_bar
		ArrayList<String> words_in_x_bar = new ArrayList<String>();
		for (String key : x_bar.words) {
			words_in_x_bar.add(key);
		}

		int k = 30; 
		//while there are still words in x_bar do
		while(!words_in_x_bar.isEmpty()){
			//select k words randomly from x_bar
			for(int select = 0; select<k;select++){
				int wordSelected = randomCluster.nextInt(words_in_x_bar.size()-1);
				int clusterSelected = randomCluster.nextInt();

				//reassign word randomly to y_bar(cluster[0]) and z_bar(cluster[1])
				if((clusterSelected%2 == 0 && iterator_cluster_one < clusterSize) || iterator_cluster_two > clusterSize){
					this.all_clusters.get(index_cluster_1).addWord(words_in_x_bar.get(wordSelected),x_bar.word_count.get(words_in_x_bar.get(wordSelected)));
					iterator_cluster_one++;
				}
				else{
					this.all_clusters.get(index_cluster_2).addWord(words_in_x_bar.get(wordSelected),x_bar.word_count.get(words_in_x_bar.get(wordSelected)));
					iterator_cluster_two++;
				}
				words_in_x_bar.remove(wordSelected);
			}
			optimize(index_cluster_1, index_cluster_2);
			k = (int) (Math.sqrt(2)*k);
		}

		// we dont need the cluster that we split anymore
		this.all_clusters.remove(x_bar);

		System.out.println("-----------------------------");
	}





	/**
	 * method to optimize two clusters of words
	 * @param x first cluster
	 * @param y second cluster
	 */
	public void optimize(int x,int y){
	    	System.out.println("starting H = " + calculate_h_forward());

		float h_max = 0;
		int iter_x = this.all_clusters.get(x).words.size();
		int iter_y = this.all_clusters.get(y).words.size();

		for(int cluster_1 = 0 ;cluster_1<iter_x;cluster_1++){
			for(int cluster_2=0;cluster_2<iter_y;cluster_2++){
				//switch words in cluster x and y and then optimize
				String word_x = this.all_clusters.get(x).words.get(cluster_1);
				String word_y = this.all_clusters.get(y).words.get(cluster_2);
				int probability_x = this.all_clusters.get(x).removeWord(word_x);
				int probability_y = this.all_clusters.get(y).removeWord(word_y);

				this.all_clusters.get(x).addWord(word_y, probability_y);
				this.all_clusters.get(y).addWord(word_x, probability_x);

				if(cluster_1==0 && cluster_2==0){
					h_max = calculate_h_forward();
					System.out.println("optimizing cluster " + x + " and " + y + ", H = " + h_max);
				}
				else{
					float h_test = calculate_h_forward();
					if(h_test < h_max){
						//update max
						h_max = h_test;
						System.out.println("swap " + word_x + " and " + word_y + " => H = " + h_max);
					}
					else{
						//unswap a and b
						int probability_unswap_y = this.all_clusters.get(x).removeWord(word_y);
						int probability_unswap_x = this.all_clusters.get(y).removeWord(word_x);

						this.all_clusters.get(x).addWord(word_x, probability_unswap_x);
						this.all_clusters.get(y).addWord(word_y, probability_unswap_y);
					}
				}

			}
		}
	}

	//bigrams contain the next words and not previous
	public float calculate_h_forward(){
		float total_prob = 0;

		//we iterate over all the clusters
		for(Cluster a_bar: all_clusters){
			//number of words in a_bar
			float words_in_a_bar = 0;

			for(String word_occurence:a_bar.words){
				words_in_a_bar+=a_bar.word_count.get(word_occurence);
			}
			// we calculate the probability of a_bar to use later in the method

			float prob_a_bar = prob_a_bar(a_bar, total_num_words);
			float sum_over_b = 0;
			//iterate through all words in the input text to JCluster
			HashMap<String, Integer> counts = new HashMap<>();
			
			for(String all_words_in_text:a_bar.words){
				//we want all previous words of b to check if any of those words are in a. meaning b comes after that word
				Set<String> check_words=info_all_words.followingWords(all_words_in_text);
				//variable to count all words in a_bar where b follows
				
				if(check_words != null){
					//check all previous words for each word in the text and check if the previous words are in the cluster a_bar
					for(String compare_cluster : check_words){
						if(counts.containsKey(compare_cluster)){
							int count = counts.get(compare_cluster);
							counts.remove(compare_cluster);
							counts.put(compare_cluster, count+this.info_all_words.count(all_words_in_text, compare_cluster));
						}
						else{
							counts.put(compare_cluster, this.info_all_words.count(all_words_in_text, compare_cluster));
						}

					}	
				}
				
			}
			
			for(String word: counts.keySet()){
				int count_ab = counts.get(word);
				float p_b_given_a = (float)count_ab / (float)words_in_a_bar;
				float calculation = p_b_given_a * ((float)Math.log(p_b_given_a)/(float)Math.log(2.0));
				if(count_ab != 0 && words_in_a_bar !=0)	{
					sum_over_b += calculation;
				}					
			}
			total_prob += sum_over_b*-1*prob_a_bar;
		}
		return total_prob;
	}

	
	/**
	 * Method used to calculate the probability of a_bar
	 * @param a_bar is a word cluster
	 * @return
	 */
	public float prob_a_bar(Cluster a_bar, int total_words){
		// variable to count the number of times a word appears in the text
		int word_count = 0;
		for(String word_tokens : a_bar.words){
			// we keep track of the times a word in this cluster appears in the text
			word_count += a_bar.word_count.get(word_tokens);
		}
		return ((float)(word_count)/(float)(total_words));
	}


	/**
	 * Method to print cluster. This method will sort the items depending on the times they appear on the text
	 * @param print
	 */
	public static void printCluster(ArrayList<Cluster> print){
		for(Cluster par: print){
		        par.word_count.print();
			System.out.println("   ");
		}
	}
	
	/**
	 * Method to print a limited amount o items in the cluster
	 * @param limit the amount of items to print in the cluster
	 */
	public static void printClusterLimited(ArrayList<Cluster> print,int limit){
		int count = 0;
		for(Cluster par: print){
			for(String ca :par.words){
				System.out.println(ca);
				if(count >= limit){
					break;
				}
				count++;
			}
			count =0;
			System.out.println("   ");
		}
	}

	public static void printCluster_single(Cluster print){
		for(String ca :print.words){
			System.out.println(ca);

		}
		System.out.println("   ");
	}
}
