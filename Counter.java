import java.util.*;
import java.util.Map.Entry;

public class Counter<T> {
    private HashMap<T, Integer> _count;
    private int _totalcount;

    public Counter() {
	_count = new HashMap<>();
	_totalcount = 0;
    }

    /*
     * Return the count of word x.
     */
    public int getCount(T x) {
	if (_count.containsKey(x))
	    return _count.get(x);
	else
	    return 0;
    }
    /*
     * Alias for getCount.
     */
    public int get(T x) { return getCount(x); }

    /*
     * Return the total count of all words.
     */
    public int getTotalCount() { return _totalcount; }

    /*
     * Return the number of word types.
     */
    public int size() { return _count.size(); }
    
    /*
     * Add to the count of word x.
     */
    public void addCount(T x, int count) {
	_count.put(x, getCount(x)+count);
	_totalcount += count;
    }

    /*
     * Add 1 to the count of word x.
     */
    public void incrementCount(T x) { addCount(x, 1); }

    public void clear() {
	_count.clear();
	_totalcount = 0;
    }

    public int remove(T x) {
	int count = getCount(x);
	_totalcount -= count;
	_count.remove(x);
	return count;
    }
    
    public float getProbability(T x) { return (float)getCount(x) / getTotalCount(); }

    public boolean containsKey(T x) { return _count.containsKey(x); }
    
    public Set<T> keySet() { return _count.keySet(); }
    
    /*
     * Sort words by frequency.
     * @return A Map from words to counts.
     */

    // from: http://stackoverflow.com/questions/8119366/sorting-hashmap-by-values
    public Map<T, Integer> sortedMap()
	{

	    List<Entry<T, Integer>> list = new LinkedList<Entry<T, Integer>>(_count.entrySet());

		// Sorting the list based on values
		Collections.sort(list, new Comparator<Entry<T, Integer>>()
				{
			public int compare(Entry<T, Integer> o1,
					Entry<T, Integer> o2)
			{
				return o2.getValue().compareTo(o1.getValue());
			}
				});

		// Maintaining insertion order with the help of LinkedList
		Map<T, Integer> sortedMap = new LinkedHashMap<T, Integer>();
		for (Entry<T, Integer> entry : list)
		{
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

    public void print()
    {
	Map<T, Integer> map = sortedMap();
    	int total_sum =0;
        for (Entry<T, Integer> entry : map.entrySet())
        {
            System.out.println("Key : " + entry.getKey().toString() + " Value : "+ entry.getValue());
            total_sum+= entry.getValue();
        }
        System.out.println("The sum is: "+ total_sum);
    }


}