\documentclass{article}

\title{JCluster}

\usepackage{amsmath}
\usepackage{algpseudocode}
\usepackage{url}
\usepackage{cancel}

\begin{document}

\maketitle

Let $a, b$ range over words and $\bar{a}, \bar{b}$ range over word clusters.
JCluster tries to find a clustering of the vocabulary that minimizes the conditional entropy of the model $P(b \mid \bar{a})$ (the probability of word $b$ after any word in cluster $\bar{a}$). The conditional entropy is:
\begin{equation}
H = - \sum_{\bar{a}} P(\bar{a}) \sum_b P(b \mid \bar{a}) \log P(b \mid \bar{a}).
\end{equation}
(I'm not 100\% sure about the $P(\bar{a})$, but it is the right thing to do and I'm assuming it's what JCluster does.)
Intuitively, this is a measure of how well the model can predict the next word given the previous cluster (lower is better). Better clusterings lead to better predictions.

Example:
\begin{center}
\verb|<s>| the cat sat on the mat ! ! ! \verb|</s>|
\\[2ex]
\begin{tabular}{cc}
cluster 0 & cluster 1 \\
\verb|<s>| & the \\
on & sat \\
cat & ! \\
mat & 
\end{tabular}
\end{center}
Note that \verb|</s>| doesn't need a cluster because it never appears as the context of another word.
\begin{align*}
P(0) &= 0.4 & P(1) &= 0.6 \\
P(\text{the} \mid 0) &= 2/4 & P(\text{the} \mid 1) &= 0 \\
P(\text{cat} \mid 0) &= 0 & P(\text{cat} \mid 1) &= 1/6 \\
P(\text{sat} \mid 0) &= 1/4 & P(\text{sat} \mid 1) &= 0 \\
P(\text{on} \mid 0) &= 0 & P(\text{on} \mid 1) &= 1/6 \\
P(\text{mat} \mid 0) &= 0 & P(\text{mat} \mid 1) &= 1/6 \\
P(\text{!} \mid 0) &= 1/4 & P(\text{!} \mid 1) &= 2/6 \\
P(\texttt{</s>} \mid 0) &= 0 & P(\texttt{</s>} \mid 1) &= 1/6
\end{align*}
\begin{align*}
H &= - 0.4 \left( 0.5 \log 0.5 + (0.25 \log 0.25) \times 2 \right) - 0.6 \left( (0.167 \log 0.167) \times 4 + 0.333 \log 0.333 \right) \\
&= 1.952.
\end{align*}

The algorithm starts with all words in a single cluster, then repeatedly divides each cluster into two smaller, equal-sized clusters until the desired number of clusters is reached. Then there is a final fine-tuning pass.
\begin{algorithmic}
\Procedure{JCluster}{$V, k$}
  \State{Assign all words in $V$ to a single cluster $\bar{x}$}
  \For{$i \leftarrow 1, \ldots, \log_2{k}$}
    \For{each cluster $\bar{x}$}
      \State{\Call{Split}{$\bar{x}$}}
    \EndFor
  \EndFor
  \For{$i \leftarrow 1, \ldots, 4$}
    \State{\Call{FineTune}{}}
  \EndFor
\EndProcedure
\end{algorithmic}

\bigskip\noindent
To split a cluster into two clusters, the basic idea is to split randomly, then repeatedly swap words and see if $H$ improves.
\begin{algorithmic}
\Procedure{Split}{$\bar{x}$}
  \State{Make two new clusters $\bar{y}$ and $\bar{z}$}
  \State{Reassign words in $\bar{x}$ randomly to $\bar{y}$ and $\bar{z}$}
  \State{\Call{Optimize}{$\bar{y}, \bar{z}$}}
\EndProcedure
\end{algorithmic}
\begin{algorithmic}
\Procedure{Optimize}{$\bar{x}, \bar{y}$}
  \State{$H_{max} \leftarrow H(\bar{x}, \bar{y})$}
  \For{each $a \in \bar{x}$, $b \in \bar{y}$}
    \State{Swap $a$ and $b$}
    \If{$H(\bar{x}, \bar{y}) > H_{max}$}
      \State{$H_{max} \leftarrow H(\bar{x}, \bar{y})$} \Comment{how does this affect the iteration over $a,b$?}
    \Else
      \State{Unswap $a$ and $b$}
    \EndIf
  \EndFor
\EndProcedure
\end{algorithmic}
The comment above is important -- this is the part I'm the most uncertain about. The problem is that we're changing a container while we're iterating over it, which is weird. Is there another way that makes more sense?

There are a couple of tricks used to improve the performance. First, use the following as a replacement for $\Call{Split}$; this version mixes the words in a little at a time:
\begin{algorithmic}
\Procedure{SplitBuckshot}{$\bar{x}$}
  \State{Make two new clusters $\bar{y}$ and $\bar{z}$}
  \State{$k\leftarrow 30$} \Comment{might need to experiment with this value}
  \While{there are still words in $\bar{x}$}
    \State{Pick $k$ words randomly from $\bar{x}$}
    \State{Reassign them randomly to $\bar{y}$ and $\bar{z}$}
    \State{\Call{Optimize}{$\bar{y}, \bar{z}$}}
    \State{$k \leftarrow \sqrt{2} \cdot k$}
  \EndWhile
\EndProcedure
\end{algorithmic}

\bigskip\noindent
The other trick is the fine-tuning phase, which considers swaps between all possible pairs of clusters:
\begin{algorithmic}
\Procedure{FineTune}{}
  \For{each pair of clusters $\bar{x}, \bar{y}$}
    \State{\Call{Optimize}{$\bar{x}, \bar{y}$}}
  \EndFor
\EndProcedure
\end{algorithmic}

There is an important optimization which allows $H$ to be recomputed quickly when a small change is made to a clustering. Let's define a few new symbols:
\begin{align*}
H_{\bar{a}} &= -c(\bar{a}) \sum_b P(b \mid \bar{a}) \log P(b \mid \bar{a}) \\
&= -\sum_b c(\bar{a}b) \log \frac{c(\bar{a} b)}{c(\bar{a})} \\
H_{\bar{a}b} &= -c(\bar{a}b) \log \frac{c(\bar{a} b)}{c(\bar{a})}
\intertext{so that}
H &= \frac1n \sum_{\bar{a}} H_{\bar{a}} \\
H_{\bar{a}} &= \sum_b H_{\bar{a}b}.
\end{align*}
Suppose we add word $x$ to $\bar{a}$, yielding new counts $c'$. Then
\begin{align*}
H'_{\bar{a}} - H_{\bar{a}} &= \sum_b \left( H'_{\bar{a}b} - H_{\bar{a}b} \right) \\
&= \sum_{b\mid c(xb)>0} \left( H'_{\bar{a}b} - H_{\bar{a}b} \right) + \sum_{b\mid c(xb)=0} \left( H'_{\bar{a}b} - H_{\bar{a}b} \right)
\end{align*}
The first sum is easy to compute. Let's take a closer look at the second:
\begin{align*}
\sum_{b\mid c(xb)=0} \left( H'_{\bar{a}b} - H_{\bar{a}b} \right) 
&= -\sum_{b\mid c(xb)=0} \left( c'(\bar{a}b) \log \frac{c'(\bar{a} b)}{c'(\bar{a})} - c(\bar{a}b) \log \frac{c(\bar{a} b)}{c(\bar{a})} \right) \\
&= -\sum_{b\mid c(xb)=0} \left( \cancel{c'(\bar{a}b) \log c'(\bar{a} b)} - c'(\bar{a}b) \log c'(\bar{a}) - \cancel{c(\bar{a}b) \log c(\bar{a} b)} + c(\bar{a}b) \log c(\bar{a}) \right) \\
&= -\sum_{b\mid c(xb)=0} \left(- c'(\bar{a}b) \log c'(\bar{a}) + c(\bar{a}b) \log c(\bar{a}) \right) \\
&= -\sum_{b\mid c(xb)=0} c(\bar{a}b) (- \log c'(\bar{a}) + \log c(\bar{a})) \\
&= -\sum_{b\mid c(xb)=0} c(\bar{a}b) \log \frac{c(\bar{a})}{c'(\bar{a})} \\
&= -\left(c(\bar{a}) - \sum_{b\mid c(xb)>0} c(\bar{a}b) \right) \log \frac{c(\bar{a})}{c'(\bar{a})} 
\end{align*}
which is also easy to compute. So the change in entropy caused by adding $x$ to $\bar{a}$ is
\begin{equation}
H'_{\bar{a}} - H_{\bar{a}} = \sum_{b\mid c(xb)>0} \left( H'_{\bar{a}b} - H_{\bar{a}b} \right) - \left(c(\bar{a}) - \sum_{b\mid c(xb)>0} c(\bar{a}b) \right) \log \frac{c(\bar{a})}{c'(\bar{a})} 
\end{equation}
and the change in entropy caused by removing $x$ from $\bar{a}$ is the negative of the above expression. 

\section*{References}

\begin{trivlist}
\item Joshua T. Goodman, 2001. A bit of progress in language modeling: extended version. MSR Technical Report MSR-TR-2001-72. Appendix B.4.
\item Joshua Goodman, 2002. JCLUSTER documentation. \url{http://bit.ly/1zIZxCn}
\end{trivlist}



\end{document}